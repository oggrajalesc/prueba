
package com.prueba.exeption;

public class ServiceException extends Exception {

	
	private static final long serialVersionUID = 1L;
	private String codigosError;
	
	/**
	 * Constructor vacio
	 */
	public ServiceException() {
		super();
	}


	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 * @param codigosError
	 */
	public ServiceException(String message, Throwable cause,String codigosError)  {
		super(message, cause);
		this.setCodigosError(codigosError);
	}

	/**
	 * 
	 * @param cause
	 */
	public ServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 * @param message
	 * @param codigosError
	 */
	public ServiceException(String message,String codigosError) {
		super(message);
		this.setCodigosError(codigosError);
	}


	/**
	 * 
	 * @return
	 */
	public String getCodigosError() {
		return codigosError;
	}


	/**
	 * 
	 * @param codigosError
	 */
	public void setCodigosError(String codigosError) {
		this.codigosError = codigosError;
	}

}
