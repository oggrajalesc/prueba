package com.prueba.web.rest.util;

public interface Constantes {

	
	enum CodigoError{
		ARCHIVOVACIO("1"),
		ERROTMAYOR("2"),
		ERRORNMATOY("3"), 
		DATAINCORRECTA("4");
		String codigo;
		
		CodigoError(String codigo){
			this.codigo = codigo;
		}
		
		public String getCodigo(){
			return codigo;
		}
	}
}
