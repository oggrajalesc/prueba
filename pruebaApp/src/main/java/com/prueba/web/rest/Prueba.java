package com.prueba.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.prueba.exeption.ServiceException;
import com.prueba.service.PruebaService;

@RestController
@RequestMapping("api")
public class Prueba {

	@Autowired PruebaService pruebaService;
	
	@RequestMapping("cargarArchivo" )
	@CrossOrigin
	public  ResponseEntity<Resource> cargarArchivoDatosTrabajo(@RequestParam(value = "file") MultipartFile fileMultipar,
			@RequestParam(value = "dni") String dni) throws ServiceException{
		return pruebaService.calcularNumeroDeViajes(fileMultipar,dni);
		
		
	}
}