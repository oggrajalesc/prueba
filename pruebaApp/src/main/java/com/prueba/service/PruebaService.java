package com.prueba.service;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.prueba.exeption.ServiceException;

public interface PruebaService {

	ResponseEntity<Resource> calcularNumeroDeViajes(MultipartFile fileMultipar, String dni) throws ServiceException;

}
