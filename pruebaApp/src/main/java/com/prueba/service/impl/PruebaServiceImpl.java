package com.prueba.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.prueba.exeption.ServiceException;
import com.prueba.service.PruebaService;
import com.prueba.web.rest.util.Constantes;



@Service
public class PruebaServiceImpl implements PruebaService{

	private static final String DATA_ENVIDA_DE_MANERA_INCORECTA = "Data envida de manera incorecta";
	private static final String EL_CAMPO_N_ELEMENTOS_ES_ERRADO_100 = "el campo n elementos es errado > 100";
	private static final String ERROR_AL_OBTENER_DATA_DEL_ARCHIVO = "error al obtener data del archivo";
	private static final String EL_NUMERO_DE_DIAS_TRABAJADOS_ES_MAYOR_QUE_500 = "El numero de dias trabajados es mayor  que 500";
	private static final String LISTADO_DE_ELEMENTOS_VACIO = "listado de elementos vacio";

	private int diasAtrabajar;
	private List<Integer> listaN;
	private List<List<Integer>> listaElementos;
	private List<Integer> resultado;
	
	@Override
	public ResponseEntity<Resource> calcularNumeroDeViajes(MultipartFile fileMultipar, String dni)
			throws ServiceException {
		iniciarVaribales();
		List<Integer> listaElementos = leerArchivo(fileMultipar);
		validarArchivo(listaElementos);
		calcularNumeroDeViajes();
 		return generarArchivo(resultado, diasAtrabajar);
	}

	private void iniciarVaribales() {
		diasAtrabajar = 0;
		listaN = new ArrayList<>();
		listaElementos = new ArrayList<>();
		resultado = new ArrayList<>();
	}

	private void calcularNumeroDeViajes() {
		for (List<Integer> sublistaElementos : listaElementos) {
			int contador = 0;
			
			Collections.sort(sublistaElementos ,Collections.reverseOrder());
			int contadorElementosRetirados = 0;
			for (Integer integer : sublistaElementos) {
				List<Integer> bolsaOpaca = new ArrayList<>();
			
				BigDecimal bd = new BigDecimal(50).divide(new BigDecimal(integer),RoundingMode.UP);
				double totalElementos = bd.setScale(0, RoundingMode.UP).intValue();
				contadorElementosRetirados += totalElementos;
				if(contadorElementosRetirados <=sublistaElementos.size()){
					bolsaOpaca.add(integer);
					if(integer<50){
						bolsaOpaca.addAll(sublistaElementos.subList(Integer.valueOf((int) (sublistaElementos.size()-(totalElementos-1))), sublistaElementos.size()));
					}
					if(bolsaOpaca.size()== (int)totalElementos){
						contador++;
					}
				}else{
					break;
				}
			}
			this.resultado.add(contador);
		}
	}

	private void validarArchivo(List<Integer> listaElementos) throws ServiceException {
		validarDiasLaborados(listaElementos);
		for (int i = 0; i < listaElementos.size(); i++) {
			if(listaElementos.get(i) == null || listaElementos.get(i)>100){
				throw new ServiceException(EL_CAMPO_N_ELEMENTOS_ES_ERRADO_100, Constantes.CodigoError.ERRORNMATOY.getCodigo());
			}else{
				int j = i;
				try{
					this.listaElementos.add(listaElementos.subList(j+1, j+listaElementos.get(j)+1));
				}catch(IndexOutOfBoundsException e){
					throw new ServiceException(DATA_ENVIDA_DE_MANERA_INCORECTA,e,Constantes.CodigoError.DATAINCORRECTA.getCodigo());
				}
				listaN.add(listaElementos.get(i));
				i=i+listaElementos.get(i);
			}
		}
	}

	private void validarDiasLaborados(List<Integer> listaElementos) throws ServiceException {
		
		if(listaElementos == null || listaElementos.isEmpty()){
			throw new ServiceException(LISTADO_DE_ELEMENTOS_VACIO,Constantes.CodigoError.ARCHIVOVACIO.getCodigo());
		}else if(listaElementos.get(0)>500){
			throw new ServiceException(EL_NUMERO_DE_DIAS_TRABAJADOS_ES_MAYOR_QUE_500, Constantes.CodigoError.ERROTMAYOR.getCodigo());
		}else{
			diasAtrabajar = listaElementos.get(0);
			listaElementos.remove(0);
		}
	}

	private List<Integer> leerArchivo(MultipartFile fileMultipar) throws ServiceException {
		List<Integer> result = new ArrayList<>();
		try {
			
		     String line;
		     InputStream is = fileMultipar.getInputStream();
		     BufferedReader br = new BufferedReader(new InputStreamReader(is));
		     while ((line = br.readLine()) != null) {
		          result.add(Integer.valueOf(line));
		          
		     }

			
		} catch (IOException e) {
			throw new ServiceException(ERROR_AL_OBTENER_DATA_DEL_ARCHIVO,e, Constantes.CodigoError.DATAINCORRECTA.getCodigo());
		}
		return result;
	}

	
	public ResponseEntity<Resource> generarArchivo(List<Integer> resultado, int n) throws ServiceException {
		String nombreArchivo = "lazy_loading_example_output.txt";
		File tempFile;
		try {
			FileWriter fichero = new FileWriter(nombreArchivo);
			PrintWriter pw = new PrintWriter(fichero);

			int caso;
			for (int i = 0; i < n; i++) {
				caso = i + 1;
				pw.println("Case #" + caso + ": " + resultado.get(i));
			}
			fichero.close();
			tempFile = new File(nombreArchivo);
		} catch (Exception e) {
			throw new ServiceException( "Error al escribir dato de salida",e,e.getMessage());
		}
		
		Resource file = new FileSystemResource(tempFile);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + nombreArchivo + "\"").body(file);
		

	}
}
