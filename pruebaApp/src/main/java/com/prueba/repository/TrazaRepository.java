package com.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.domain.PersistentAuditEvent;

@Repository
public interface TrazaRepository extends JpaRepository<PersistentAuditEvent,Long>{

}
