package com.prueba.web.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import com.prueba.PruebaApp;
import com.prueba.exeption.ServiceException;
import com.prueba.web.rest.util.Constantes;

import io.undertow.server.handlers.resource.FileResource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PruebaApp.class)
public class PruebaRestTest {

	@Autowired
	private Prueba prueba;

	@Test
	public void cargarArchivo() throws Exception {
		InputStream uploadStream = PruebaRestTest.class.getResourceAsStream("lazy_loading_example_input.txt");
		MockMultipartFile file = new MockMultipartFile("file", "lazy_loading_example_input.txt", "multipart/form-data",
				uploadStream);
		ResponseEntity<Resource> respuesta = prueba.cargarArchivoDatosTrabajo(file, "2");
		Assert.assertNotNull(respuesta);
		Resource fileResource = respuesta.getBody();
		File fileRespuesta = fileResource.getFile();
		leerArchivo(fileRespuesta);
	}

	
	private void leerArchivo(File fileMultipar) throws Exception {
		List<String> result = new ArrayList<>();
		String line;
		InputStream is = new FileInputStream(fileMultipar);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		while ((line = br.readLine()) != null) {
			result.add(line);
		}
		Assert.assertEquals(result.get(0),"Case #1: 2");
		Assert.assertEquals(result.get(1),"Case #2: 1");
		Assert.assertEquals(result.get(2),"Case #3: 2");
		Assert.assertEquals(result.get(3),"Case #4: 3");
		Assert.assertEquals(result.get(4),"Case #5: 8");
		
	}



	@Test
	public void cargarArchivoTMayor() throws IOException, ServiceException {
		boolean error = false;
		try{
			InputStream uploadStream = PruebaRestTest.class.getResourceAsStream("errorTMayor.txt");
			MockMultipartFile file = new MockMultipartFile("file", "lazy_loading_example_input.txt", "multipart/form-data",
					uploadStream);
			prueba.cargarArchivoDatosTrabajo(file, "2");
		}catch(ServiceException e){
			Assert.assertEquals(e.getCodigosError(), Constantes.CodigoError.ERROTMAYOR.getCodigo());
			error = true;
		}
		Assert.assertTrue(error);
	}	
	
	@Test
	public void cargarArchivoNMayor() throws IOException, ServiceException {
		boolean error = false;
		try{
			InputStream uploadStream = PruebaRestTest.class.getResourceAsStream("errorNMayor.txt");
			MockMultipartFile file = new MockMultipartFile("file", "lazy_loading_example_input.txt", "multipart/form-data",
					uploadStream);
			prueba.cargarArchivoDatosTrabajo(file, "2");
		}catch(ServiceException e){
			Assert.assertEquals(e.getCodigosError(), Constantes.CodigoError.ERRORNMATOY.getCodigo());
			error = true;
		}
		Assert.assertTrue(error);			
		
	}	

	
	@Test
	public void cargarArchivoDataIncorrecta() throws IOException, ServiceException {
		boolean error = false;
		try{
			InputStream uploadStream = PruebaRestTest.class.getResourceAsStream("badData.txt");
			MockMultipartFile file = new MockMultipartFile("file", "lazy_loading_example_input.txt", "multipart/form-data",
					uploadStream);
			prueba.cargarArchivoDatosTrabajo(file, "2");
		}catch(ServiceException e){
			Assert.assertEquals(e.getCodigosError(), Constantes.CodigoError.DATAINCORRECTA.getCodigo());
			error = true;
		}
		Assert.assertTrue(error);			
		
	}	
	
}
