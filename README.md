# README #
prueba de desarrollo: 

Wilson trabaja para una compañía de mudanzas. Su principal tarea es cargar            elementos del hogar en un camión. Wilson tiene una bolsa que usa para mover              estos elementos. Él coloca un puñado de elementos en la bolsa, luego los mueve              al camión y los lleva al destino. 
 
Wilson tiene la reputación de ser un trabajador perezoso. Julie es la supervisora             de Wilson y es la encargada de que Wilson haga bien su trabajo. Ella quiere que                Wilson lleve al menos 50 libras de elementos en su bolsa cada vez que él vaya al                 camión. 
 

 
 
Por suerte para Wilson, su bolsa es opaca, sin embargo cuando él lleva una bolsa               llena de elementos, Julie puede descifrar cuántos elementos está cargando          (basada en la altura de la pila de elementos en la bolsa) y también el peso del                 elemento que está en la parte superior de la pila. Sin embargo ella no puede               descifrar cuánto pesan los demás elementos de la bolsa. Ella asume que cada             elemento en la bolsa pesa al menos tanto como el elemento que está en la parte                superior, porque seguramente Wilson no sería tan tonto como para colocar           elementos más pesados encima de elementos menos pesados. Lamentablemente         Julie es ignorante de la gran falta de dedicación de Wilson en sus tareas por lo                cual sus suposiciones son frecuentemente incorrectas. 
 
Hoy hay ​N elementos para ser movidos y Wilson, a quien se le paga por hora,                quiere maximizar el número de viajes que tiene que hacer para colocar todos los              elementos en el camión, de esta manera le pagarán más ¿cuál es número de              viajes máximo que Wilson puede hacer sin ser sorprendido por Julie? 
 
Cabe notar que Julie no es consciente de cuántos elementos van a ser movidos              hoy y ella no mantiene la cuenta de cuántos elementos Wilson ha movido cuando              examina cada bolsa. Ella simplemente asume que cada bolsa llena de elementos            contiene al menos un peso total de​k * w donde​k​es el número de elementos en                  la bolsa y ​w​ es el peso del elemento que está en la parte superior. 
 
Entrada. 
 
La entrada comienza con un entero​T​, el número de días que Wilson “trabaja” en               su trabajo. Para cada día hay una línea que contiene el entero​N​, después hay​N                líneas, donde cada línea subsiguiente contiene el peso del item ​Wi  
 
Salida 
 
Para cada día ​i​, imprimir una línea que contenga “Case #i:” seguido del máximo              número de viajes que Wilson puede hacer ese día. 

 
 
 
Restricciones. 
 
1 ≤​ T​ ≤ 500  1 ≤ ​N​ ≤ 100  1 ≤ ​Wi​ ≤ 100  
 
En cada día, es garantizado que el peso total de todos los elementos es al menos                50 libras.

### What is this repository for? ###


### How do I get set up? ###

* para correr el back gradlew bootRun en la raiz del proyecto
*el proyecto cuenta con profiles de dev y prod 
* se utiliza una base de datos h2 para almacenar la trazabilidad solicitada http://localhost:8081/h2-console
*para la creacion de la base de datos se utiliza liquid base.
* para la trazabilidad de los eventos se utilizo aspecj poniendo un proxy a los paquetes com.prueba.web.rest-com.prueba.service-com.prueba.repository 
* el servicio expuesto corre en: http://localhost:8081/api/cargarArchivo
* el proyecto se importa a eclipse utilizando gradle como un gradlePoject
* se utiliza junit para ejecutar los test unitarios
* el proyecto cuenta con el plugin de sonar el cual corre con el siguiente comando gradlew sonarqube -Dsonar.organization=oggrajalesc-bitbucket -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=.
* el sonar utilizado es el de la nube publica https://sonarcloud.io.
* el proyecto utiliza docker gradlew bootWar -Pprod buildDocker (Pprod profile de produccion)
* para desplegar en una nube publica basta con levantar un Kubernetes en la nube deseada y orquestar las imagenes y contenedores 


###frond end #
* se debe tener npm previamente instalado y angular cli
* para ejecutar el frond se debe parar en la raiz del proyecto y ejecutar el comando ng serve --open o npm start

### Solucion ###
5   T
4   N1
30     1
30     2
1      3
1      4
3   N2
20		1
20		2
20		3
11  N3
1		1
2		2
3		3
4		4
5		5
6		6
7		7
8		8
9		9
10		10
11		11
6	N4
9		1
19		2
29		3
39		4
49		5
59		6
10	N5
32		1
56		2
76		3
8		4
44		5
60		6
47		7
85		8
71		9
91		10


1. Viaje minimo de 50 libras


2. Pila de elementos:

	* Se supone las n (k) lineas apiladas de elementos que contiene la bolsa.
	* Se conoce el peso W del primer elemento de la pila.
	* Julie supone que todas las lineas apiladas son del mismo peso (wrong)
	 o que el elemento inferior es mas pesado que 
	 el superior
	 
3. N son la cantidad total de elementos a ser transportados en un día

4. Julie desconoce N

5. Julie desconoce el historial de elementos movidos




1. Armar los sub arreglos de elementos

2. A cada array obtenido se llama funcion cargaPeresoza();



hace:

1. Ordenar array de mayor a menor
2. el mas pesado => El mas pesado
3. techo(el mas pesado / 50) => numero de elementos que meto en la bolsa (elementosAMeter)
4. Remover del array (elementosAMeter) de los elementos menos pesados
5. Iterar
6. Si quedan elementos no acomodables en paquetes de peso supuesto de 50, se meten a cualquier bolsa
3. Se debe aplicar validaciones